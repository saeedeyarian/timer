let timeBegan = null;
let timeStop = null;
let stopDuration = 0;
let startInterval = null;
let flag = false; //to control the start/stop of the timer


document.querySelector(".container").addEventListener ("click" , () => {
    if(!flag) {
        startTimer ();
        flag = true;
    } else {
        stopTimer();
        flag = false;
    }
});

document.querySelector(".container").addEventListener ("dblclick" , () => {
    resetTimer();
});

startTimer = () => {
    if(timeBegan === null){
        timeBegan = new Date();
    }
    if (timeStop !== null) {
        stopDuration += (new Date() - timeStop)
    }
    startInterval =  setInterval(showTime, 10);
}

stopTimer = () => {
    timeStop = new Date();
    clearInterval(startInterval)
}

showTime = () => {
    let date = new Date();
    let timeElapsed = new Date(date - timeBegan - stopDuration)
 
    let minuts = timeElapsed.getUTCMinutes();
    let second = timeElapsed.getUTCSeconds();
    let millSecond = timeElapsed.getUTCMilliseconds();
    millSecond = Math.floor(millSecond/10)
 
    minuts = (minuts < 10) ? `0${minuts}` : minuts;
    second = (second < 10) ? `0${second}` : second;
    millSecond = (millSecond < 10) ? `0${millSecond}` : millSecond;
 
    let timer = `${minuts}:${second}:${millSecond}`;
 
    document.querySelector(".show-timer").innerText = timer;
 }
 
 resetTimer = () => {
    clearInterval(startInterval)
    timeBegan = null;
    timeStop = null;
    stopDuration = 0;
    document.querySelector(".show-timer").innerText = "00:00:00";
    flag = false;
}